## Лабораторна робота 3

### Тема: "Дослідження методів багатопоточності та засобів паралельного API Java при розробки програмних компонентів розподіленної системі. Робота з пакетом java.util.concurrent"

#### Виконав: Кондратьєв Олександр

### Завдання 1
## 1) Постановка завдання
![Скріншот1](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/1.1.png?ref_type=heads)
![Скріншот2](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/1.2.png?ref_type=heads)
![Скріншот3](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/1.3.png?ref_type=heads)

## 2) Реалізація
![Скріншот4](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/2.png?ref_type=heads)
**клас Main**
---
![Скріншот5](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/3.1.png?ref_type=heads)
![Скріншот6](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/3.2.png?ref_type=heads)
**клас Mutex**
---
![Скріншот7](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/4.1.png?ref_type=heads)
![Скріншот8](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/4.2.png?ref_type=heads)
**клас Semaphore**
---
![Скріншот9](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/5.1.png?ref_type=heads)
![Скріншот10](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/5.2.png?ref_type=heads)
**клас Atomic**
---
![Скріншот11](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/6.1.png?ref_type=heads)
![Скріншот12](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/6.2.png?ref_type=heads)
**клас ThreadPool**
---
## 3) Результати виконання
![Скріншот13](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/7.1.png?ref_type=heads)
![Скріншот14](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/7.2.png?ref_type=heads)
![Скріншот15](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/7.3.png?ref_type=heads)
![Скріншот16](https://gitlab.com/oleksandrkondratiev/lab-3/-/raw/main/images/7.4.png?ref_type=heads)
---