import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MutexClass implements Runnable{
    private static final Lock mutex = new ReentrantLock();
    private static final Random random = new Random();

    public void run() {
        int n = getIntegerInput("Введіть кількість потоків: ");
        int t1 = getIntegerInput("Введіть мінімальний час очікування (мс): ");
        int t2 = getIntegerInput("Введіть максимальний час очікування (мс): ");

        // Створення та запуск потоків
        for (int i = 0; i < n; i++) {
            Thread thread = new Thread(() -> {
                String threadName = Thread.currentThread().getName();

                // Очікування доступу до м'ютексу
                mutex.lock();
                try {
                    System.out.printf("%s: Очікування доступу до м'ютексу...\n", threadName);
                    Thread.sleep(random.nextInt(t2 - t1) + t1); // Сон на випадковий час
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // Доступ до м'ютексу
                System.out.printf("%s: Захоплення м'ютексу...\n", threadName);
                try {
                    Thread.sleep(random.nextInt(1000)); // Імітація роботи з м'ютексом
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.printf("%s: Звільнення м'ютексу...\n", threadName);

                // Звільнення м'ютексу
                mutex.unlock();
            });
            thread.setName("Поток " + (i + 1));
            thread.start();
        }
    }

    private static int getIntegerInput(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(message);
        while (!scanner.hasNextInt()) {
            System.out.println("Неправильний формат вводу. Введіть ціле число:");
            scanner.next();
        }
        return scanner.nextInt();
    }
}
