import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

public class SemaphoreClass implements Runnable{
    private static final Semaphore semaphore = new Semaphore(4); // Семафор з лічильником 4
    private static final Random random = new Random();

    @Override
    public void run() {
        int n = getIntegerInput("Введіть кількість потоків: ");
        int t1 = getIntegerInput("Введіть мінімальний час очікування (мс): ");
        int t2 = getIntegerInput("Введіть максимальний час очікування (мс): ");

        // Створення та запуск потоків
        for (int i = 0; i < n; i++) {
            Thread thread = new Thread(() -> {
                String threadName = Thread.currentThread().getName();
                // Отримання семафору
                System.out.printf("%s: Очікування семафору...\n", threadName);
                try {
                    semaphore.acquire(); // очікування дозволу
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // Доступ до ресурсу
                System.out.printf("%s: Доступ до ресурсу...\n", threadName);
                try {
                    Thread.sleep(random.nextInt(t2 - t1) + t1); // Імітація роботи з ресурсом
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.printf("%s: Звільнення семафору...\n", threadName);
                semaphore.release(); // Звільнити семафор
            });
            thread.setName("Поток " + (i + 1));
            thread.start();
        }
    }
    private static int getIntegerInput(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(message);
        while (!scanner.hasNextInt()) {
            System.out.println("Неправильний формат вводу. Введіть ціле число:");
            scanner.next();
        }
        return scanner.nextInt();
    }
}
