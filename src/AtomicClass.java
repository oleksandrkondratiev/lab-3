import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Random;

public class AtomicClass implements Runnable{
    private static final AtomicInteger counter = new AtomicInteger(0); // Блокуюча змінна
    private static final Random random = new Random();
    public void run() {
        int n;
        n = getIntegerInput("Введіть кількість потоків: ");
        // Створення та запуск потоків
        for (int i = 0; i < n; i++) {
            Thread thread = new Thread(() -> {
                String threadName = Thread.currentThread().getName();
                // Збільшення змінної
                int oldValue = counter.get();
                int newValue = counter.incrementAndGet();
                System.out.printf("%s: Збільшення змінної...%d -> %d\n", threadName, oldValue, newValue);
                // Зміна змінної
                oldValue = counter.get();
                counter.set(random.nextInt(10));
                System.out.printf("%s: Зміна змінної: %d -> %d\n", threadName, oldValue, counter.get());
                // Зменшення змінної
                oldValue = counter.get();
                newValue = counter.decrementAndGet();
                System.out.printf("%s: Зменшення змінної... %d -> %d \n", threadName, oldValue, newValue);
            });
            thread.setName("Поток " + (i + 1));
            thread.start();
        }
    }

    private static int getIntegerInput(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(message);
        while (!scanner.hasNextInt()) {
            System.out.println("Неправильний формат вводу. Введіть ціле число:");
            scanner.next();
        }
        return scanner.nextInt();
    }
}
