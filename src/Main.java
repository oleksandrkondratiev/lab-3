import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // Вибір методу синхронізації
        System.out.println("Виберіть метод синхронізації:");
        System.out.println("1. М'ютекс");
        System.out.println("2. Семафор");
        System.out.println("3. Блокуюча змінна");
        System.out.println("4. Пул потоків");
        System.out.print("Введіть номер методу: ");
        int choice = scanner.nextInt();
        switch (choice) {
            case 1 -> new MutexClass().run();
            case 2 -> new SemaphoreClass().run();
            case 3 -> new AtomicClass().run();
            case 4 -> new ThreadPoolClass().run();
            default -> System.out.println("Невідомий метод синхронізації.");
        }
        scanner.close();
    }
}
