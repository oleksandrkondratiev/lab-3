import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.Random;

public class ThreadPoolClass implements Runnable{
    public  void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введіть кількість потоків у пулі: ");
        int k = scanner.nextInt();
        System.out.print("Введіть кількість задач: ");
        int n = scanner.nextInt();
        // Створення пулу потоків з фіксованою кількістю k
        ExecutorService threadPool = Executors.newFixedThreadPool(k);
        // Запуск n задач
        for (int i = 0; i < n; i++) {
            int finalI = i;
            threadPool.execute(() -> {
                int taskId = finalI + 1;
                System.out.println("Таска " + taskId + " виконується потоком " + Thread.currentThread().getName());
                // Додаткові операції, які виконуються в задачі
                try {
                    Thread.sleep(new Random().nextInt(1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        // Зупинка пулу потоків
        shutdownThreadPool(threadPool);
        scanner.close();
    }

    private static void shutdownThreadPool(ExecutorService threadPool) {
        try {
            threadPool.shutdown();
            if (!threadPool.awaitTermination(1, TimeUnit.SECONDS)) {
                threadPool.shutdownNow();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
